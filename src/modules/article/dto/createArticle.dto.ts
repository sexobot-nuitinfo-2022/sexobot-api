import { IsNotEmpty } from 'class-validator';
import { Schema as MongooseSchema } from 'mongoose';

export class CreateArticleDto {

    @IsNotEmpty()
    sender: string;

    @IsNotEmpty()
    title: string;
    @IsNotEmpty()
    content: string;

}


