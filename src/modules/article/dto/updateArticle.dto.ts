import { PartialType } from '@nestjs/mapped-types';
import { IsNotEmpty, IsOptional } from 'class-validator';

import { CreateArticleDto } from './createArticle.dto';


export class UpdateArticleStatusDto {
    @IsNotEmpty()
    status: string;
}

export class UpdateArticleStyleDto {
    backgoundColor: string;
    textColor: string;
    textFont: string;
}
