import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { Article, ArticleSchema } from '../../entities/article.entity';
import { UserModule } from '../user/user.module';
import { ArticleController } from './article.controller';
import { ArticleService } from './article.service';
import { ArticleRepository} from './../../repositories/article.repository'

@Module({
    imports: [UserModule, MongooseModule.forFeature([{ name: Article.name, schema: ArticleSchema }])],
    controllers: [ArticleController],
    providers: [ArticleService, ArticleRepository],
    exports: [ArticleService, ArticleRepository],
})
export class ArticleModule {}
