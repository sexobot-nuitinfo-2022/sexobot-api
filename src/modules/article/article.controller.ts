import { BadRequestException, Body, Controller, Get, HttpStatus, Param, Patch, Post, Query, Res } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection, Schema as MongooseSchema } from 'mongoose';
import { GetQueryDto } from '../../dto/getQueryDto';
import { CreateArticleDto } from './dto/createArticle.dto';
import { UpdateArticleStatusDto, UpdateArticleStyleDto } from './dto/updateArticle.dto';

import { ArticleService } from './article.service';

@Controller('articles')
export class ArticleController {
    constructor(@InjectConnection() private readonly mongoConnection: Connection, private articleService: ArticleService) {}

    @Post('/')
    async createArticle(@Body() createArticleDto: CreateArticleDto, @Res() res: any) {
        const session = await this.mongoConnection.startSession();
        session.startTransaction();
        try {
            const newArticle: any = await this.articleService.createArticle(createArticleDto);
            await session.commitTransaction();
            return res.status(HttpStatus.OK).send(newArticle);
        } catch (error) {
            await session.abortTransaction();
            throw new BadRequestException(error);
        } finally {
            session.endSession();
        }
    }
    @Get('/')
    async getlistArticles(@Res() res: any) {
        
        try {
            const listArticles: any = await this.articleService.getAllArticles();
            return res.status(HttpStatus.OK).send(listArticles);
        } catch (error) {
            throw new BadRequestException(error);
        } 
    }

    @Patch('/:id/status')
    async updateArticleStatus(@Param('id') id: string,@Body() updateArticleStatusDto: UpdateArticleStatusDto, @Res() res: any) {
        const session = await this.mongoConnection.startSession();
        session.startTransaction();
        try {
            const newArticle: any = await this.articleService.updateArticleStatus(id,updateArticleStatusDto);
            await session.commitTransaction();
            return res.status(HttpStatus.OK).send(newArticle);
        } catch (error) {
            await session.abortTransaction();
            throw new BadRequestException(error);
        } finally {
            session.endSession();
        }
    }

    @Patch('/:id/style')
    async updateArticleStyle(@Param('id') id: string,@Body() updateArticleStyleDto: UpdateArticleStyleDto, @Res() res: any) {
        const session = await this.mongoConnection.startSession();
        session.startTransaction();
        try {
            const newArticle: any = await this.articleService.updateArticleStyle(id,updateArticleStyleDto);
            await session.commitTransaction();
            return res.status(HttpStatus.OK).send(newArticle);
        } catch (error) {
            await session.abortTransaction();
            throw new BadRequestException(error);
        } finally {
            session.endSession();
        }
    }

    // @Get('/getSaleById/:id')
    // async getSaleById(@Param('id') id: MongooseSchema.Types.ObjectId, @Query() getQueryDto: GetQueryDto, @Res() res: any) {
    //     const storage: any = await this.saleService.getSaleById(id);
    //     return res.status(HttpStatus.OK).send(storage);
    // }
}
