import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ClientSession, Schema as MongooseSchema } from 'mongoose';
import { ArticleRepository } from '../../repositories/article.repository';
import { UserService } from '../user/user.service';
import { CreateArticleDto } from './dto/createArticle.dto';
import { UpdateArticleStatusDto, UpdateArticleStyleDto } from './dto/updateArticle.dto';

@Injectable()
export class ArticleService {
    constructor(private articleRepository: ArticleRepository, private readonly userService: UserService) { }

    async createArticle(createArticleDto: CreateArticleDto) {

        const createdArticle = await this.articleRepository.createArticle(createArticleDto);


        return createdArticle;
    }

    async updateArticleStatus(id:string, updateArticleDto: UpdateArticleStatusDto) {

        const updatedArticle = await this.articleRepository.updateArticleStatus(id,updateArticleDto);
        return updatedArticle;
    }

    async updateArticleStyle(id:string, updateArticleStyleDto: UpdateArticleStyleDto) {

        const updatedArticle = await this.articleRepository.updateArticleStyle(id,updateArticleStyleDto);
        return updatedArticle;
    }

    async getAllArticles() {

        const listArticles = await this.articleRepository.getAllArticles();
        return listArticles;
    }

    // async getArticleById(saleId: MongooseSchema.Types.ObjectId) {
    //     return await this.saleRepository.getArticleById(saleId);
    // }

    // async getArticles(query: { from: number; limit: number }) {
    //     return await this.saleRepository.getArticles(query);
    // }
}
