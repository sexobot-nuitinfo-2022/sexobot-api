import { IsNotEmpty, IsString } from 'class-validator';

export class CreateUserDto {

    @IsString()
    @IsNotEmpty()
    username: string;

    @IsString()
    @IsNotEmpty()
    discordId: string;

    @IsString()
    @IsNotEmpty()
    discriminator: string;

    @IsString()
    @IsNotEmpty()
    avatar: string;

    @IsString()
    @IsNotEmpty()
    createdTimestamp: string;

    @IsString()
    @IsNotEmpty()
    defaultAvatarURL: string;

    @IsString()
    @IsNotEmpty()
    tag: string;

    @IsString()
    @IsNotEmpty()
    avatarURL: string;

    @IsString()
    @IsNotEmpty()
    displayAvatarURL: string;
}
