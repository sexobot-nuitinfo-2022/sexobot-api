import { Injectable } from '@nestjs/common';
import { ClientSession, Schema as MongooseSchema } from 'mongoose';
import { UserRepository } from '../../repositories/user.repository';
import { CreateUserDto } from './dto/createUser.dto';

@Injectable()
export class UserService {
    constructor(private readonly userRepository: UserRepository) {}

    async createUser(createUserDto: CreateUserDto) {
        const createdUser = await this.userRepository.createUser(createUserDto);
        return createdUser;
    }

    async getUserByDiscordId(id:string) {
        return await this.userRepository.getUserByDiscordId(id);
    }

    
}
