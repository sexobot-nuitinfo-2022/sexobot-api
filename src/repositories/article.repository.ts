import { BadRequestException, ConflictException, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ClientSession, Model, Schema as MongooseSchema } from 'mongoose';
import { UpdateArticleStatusDto, UpdateArticleStyleDto } from 'src/modules/article/dto/updateArticle.dto';
import { Article } from '../entities/article.entity';
import { CreateArticleDto } from '../modules/article/dto/createArticle.dto';

export class ArticleRepository {
    constructor(@InjectModel(Article.name) private readonly ArticleModel: Model<Article>) { }

    async createArticle(createArticleDto: CreateArticleDto) {
        let article = new this.ArticleModel({
            sender: createArticleDto.sender,
            title: createArticleDto.title,
            content: createArticleDto.content,
            textColor: "#ffffff",
            textFont:"Roboto",
            backgoundColor:"#000000",
            status: "PENDING"
        });

        try {
            let response = await article.save();
            if (!response) {
                throw new BadRequestException('Article not created');
            }
            return article;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }
    async updateArticleStatus(id: string, updateArticleStatus: UpdateArticleStatusDto) {
        const actualDate = new Date();
        actualDate.toUTCString();

        let article;
        try {
            article = await this.ArticleModel
                .findOneAndUpdate({ _id: id }, updateArticleStatus, {
                    new: true,
                })
                .exec();
        } catch (error) {
            throw new InternalServerErrorException(error);
        }

        if (!article) {
            throw new ConflictException('Error trying to update article');
        }

        return article;

    }


    async updateArticleStyle(id: string, updateArticleStyleDto: UpdateArticleStyleDto) {
        const actualDate = new Date();
        actualDate.toUTCString();

        let updatedBody = updateArticleStyleDto;
        Object.keys(updatedBody).forEach(key => {
            if (updatedBody[key] === null) {
              delete updatedBody[key];
            }
          });

        let article;
        try {
            article = await this.ArticleModel
                .findOneAndUpdate({ _id: id }, updatedBody, {
                    new: true,
                })
                .exec();
        } catch (error) {
            throw new InternalServerErrorException(error);
        }

        if (!article) {
            throw new ConflictException('Error trying to update product');
        }

        return article;

    }


    async getAllArticles() {
        let articles;
        
        try {
            articles = await this.ArticleModel
                .find({status: "APPROVED"})
                .populate('sender')
                .exec();
        } catch (error) {
            throw new InternalServerErrorException(error);
        }

        if (!articles) {
            return []
        }

        return articles;

    }





    // async getSales(query: { from: number; limit: number }) {
    //     let from = query.from || 0;
    //     from = Number(from);

    //     let limit = query.limit || 0;
    //     limit = Number(limit);

    //     let articles: Article[];

    //     try {
    //         if (limit === 0) {
    //             articles = await this.saleModel
    //                 .find()
    //                 .populate('user')
    //                 .skip(from)
    //                 .sort({ createdAt: -1 })
    //                 .exec();
    //         } else {
    //             articles = await this.saleModel
    //                 .find()
    //                 .populate('user')
    //                 .skip(from)
    //                 .limit(limit)
    //                 .sort({ createdAt: -1 })
    //                 .exec();
    //         }

    //         let response;

    //         if (sales.length > 0) {
    //             response = {
    //                 ok: true,
    //                 data: sales,
    //                 message: 'Get Sales Ok!',
    //             };
    //         } else {
    //             response = {
    //                 ok: true,
    //                 data: [],
    //                 message: 'No hay sales',
    //             };
    //         }
    //         return response;
    //     } catch (error) {
    //         throw new InternalServerErrorException(error);
    //     }
    // }

    // async getSaleById(id: MongooseSchema.Types.ObjectId) {
    //     let sale;
    //     try {
    //         sale = await this.saleModel
    //             .findById(id)
    //             .populate('product')
    //             .populate('client')
    //             .populate('user', 'name email')
    //             .exec();
    //     } catch (error) {
    //         throw new BadRequestException(error);
    //     }

    //     if (!sale) {
    //         throw new NotFoundException('Sale not found');
    //     }

    //     return sale;
    // }

    // async getSaleByProductId(productId: MongooseSchema.Types.ObjectId) {
    //     let sale;
    //     try {
    //         sale = await this.saleModel.find({ product: productId }).exec();
    //     } catch (error) {
    //         throw new InternalServerErrorException(error);
    //     }

    //     if (!sale) {
    //         throw new NotFoundException('Sale not found');
    //     }

    //     return sale;
    // }
}
