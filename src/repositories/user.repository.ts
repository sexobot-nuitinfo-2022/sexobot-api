import { ConflictException, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ClientSession, Model, Schema as MongooseSchema } from 'mongoose';
import { User } from '../entities/user.entity';
import { CreateUserDto } from '../modules/user/dto/createUser.dto';

export class UserRepository {
    constructor(@InjectModel(User.name) private readonly userModel: Model<User>) { }

    async createUser(createUserDto: CreateUserDto) {
        let user = await this.getUserByDiscordId(createUserDto.discordId);

        if (user) {
            throw new ConflictException('User already exists');
        }

        user = new this.userModel({
            username: createUserDto.username,
            discordId: createUserDto.discordId,
            discriminator: createUserDto.discriminator,
            avatar: createUserDto.avatar,
            createdTimestamp: createUserDto.createdTimestamp,
            defaultAvatarURL: createUserDto.defaultAvatarURL,
            tag: createUserDto.tag,
            avatarURL: createUserDto.avatarURL,
            displayAvatarURL: createUserDto.displayAvatarURL,
            role: "USER"
        });

        try {
            user = await user.save();
        } catch (error) {
            throw new InternalServerErrorException(error);
        }

        if (!user) {
            throw new ConflictException('User not created');
        }

        return user;
    }

    async getUserById(id: MongooseSchema.Types.ObjectId) {
        let user;
        try {
            user = await this.userModel.findById({ _id: id });
        } catch (error) {
            throw new InternalServerErrorException(error);
        }

        if (!user) {
            throw new NotFoundException('User not found');
        }

        return user;
    }

    async getUserByDiscordId(discordId: string) {
        let user;
        try {
            user = await this.userModel.findOne({ discordId }, '').exec();
        } catch (error) {
            throw new InternalServerErrorException(error);
        }

        return user;
    }
}
