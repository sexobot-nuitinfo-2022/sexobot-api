import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class User extends Document {
    @Prop({ required: true })
    username: string;

    @Prop({ required: true, unique: true })
    discordId: string;

    @Prop({ required: true })
    discriminator: string;
    @Prop({ required: true })

    avatar: string;
    @Prop({ required: true })

    createdTimestamp: string;
    @Prop({ required: true })

    defaultAvatarURL: string;
    @Prop({ required: true, unique: true })
    tag: string;
    @Prop({ required: true })

    avatarURL: string;
    @Prop({ required: true })

    displayAvatarURL: string;
    @Prop({ required: true, enum: ['ADMIN', 'USER'] })
    role: string;


    @Prop({ default: Date.now })
    createdAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
